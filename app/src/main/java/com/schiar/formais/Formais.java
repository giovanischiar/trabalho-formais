package com.schiar.formais;

import android.app.Application;

import com.schiar.formais.controller.AutomataController;
import com.schiar.formais.controller.AutomatonOperator;
import com.schiar.formais.controller.AutomatonRequisitor;
import com.schiar.formais.controller.AutomatonSaver;
import com.schiar.formais.controller.RegexController;
import com.schiar.formais.controller.RegexOperator;
import com.schiar.formais.controller.RegexRequisitor;
import com.schiar.formais.controller.RegexSaver;
import com.schiar.formais.controller.TableValidator;

/**
 * Created by Giovani on 13/11/2016.
 */

public class Formais extends Application {
    private AutomataController automataCtrl;
    private RegexController regexCtrl;


    public TableValidator getTableValidator() {
        verifyAutomataCtrl();
        return automataCtrl;
    }

    public AutomatonSaver getAutomatonSaver() {
        verifyAutomataCtrl();
        return automataCtrl;
    }

    public AutomatonRequisitor getAutomatonRequisitor() {
        verifyAutomataCtrl();
        return automataCtrl;
    }

    public AutomatonOperator getAutomatonOperator() {
        verifyAutomataCtrl();
        return automataCtrl;
    }

    public void verifyAutomataCtrl() {
        if(automataCtrl == null) {
            automataCtrl = new AutomataController(getBaseContext());
        }
    }

    public RegexSaver getRegexSaver() {
        verifyRegexCtrl();
        return regexCtrl;
    }

    public RegexOperator getRegexOperator() {
        verifyRegexCtrl();
        return regexCtrl;
    }

    public RegexRequisitor getRegexRequisitor() {
        verifyRegexCtrl();
        return regexCtrl;
    }

    public void verifyRegexCtrl() {
        if(regexCtrl == null) {
            regexCtrl = new RegexController(getBaseContext());
        }
    }

}
