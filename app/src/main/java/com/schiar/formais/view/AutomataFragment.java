package com.schiar.formais.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.schiar.formais.Formais;
import com.schiar.formais.R;
import com.schiar.formais.controller.AutomatonOperator;
import com.schiar.formais.controller.AutomatonRequisitor;
import com.schiar.formaisLib.model.FiniteAutomaton;

import java.util.ArrayList;
import java.util.List;

import static com.schiar.formais.view.AutomataFragment.IconState.COMPARATION;
import static com.schiar.formais.view.AutomataFragment.IconState.INTERSECTION;
import static com.schiar.formais.view.AutomataFragment.IconState.OPEN;

public class AutomataFragment extends Fragment {

    private AutomatonOperator automatonOperator;
    private AutomatonRequisitor automatonRequisitor;
    private String currentFA, currentFAName;
    public enum IconState {OPEN, INTERSECTION, COMPARATION};
    private IconState currentState;
    private List<String> automataIDs;
    private Snackbar bar;
    private MainActivity ma;
    private View rootView;
    private FlowLayout automataGrid;
    private boolean activityCreated;
    private boolean rootViewCreated;

    public AutomataFragment() {
        // Required empty public constructor
    }
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        automataIDs = new ArrayList<>();
        currentState = OPEN;
        activityCreated = false;
        rootViewCreated = false;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ma = (MainActivity) getActivity();
        automatonOperator = ((Formais)ma.getApplication()).getAutomatonOperator();
        automatonRequisitor = ((Formais)ma.getApplication()).getAutomatonRequisitor();
        bar = Snackbar.make(ma.findViewById(R.id.main), "", Snackbar.LENGTH_INDEFINITE);
        activityCreated = true;
        if(rootViewCreated) {
            updateFAList();
        }
    }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_automata, container, false);
        automataGrid = (FlowLayout) rootView.findViewById(R.id.automataGrid);
        rootViewCreated = true;
        if(activityCreated) {
            updateFAList();
        }
        return rootView;
    }

    public void createFAItem(final String name, final String desc, final boolean isNDFA, final String id, final String[][] table) {
        automataIDs.add(id);
        rootView.findViewById(R.id.no_fa_placeholder).setVisibility(View.GONE);
        automataGrid.setVisibility(View.VISIBLE);
        RelativeLayout faRepres = (RelativeLayout) inflateView(R.layout.view_automaton, automataGrid);
        TextView tv = (TextView) faRepres.findViewById(R.id.fa_label);
        if (isNDFA) {
            //((TextView) faRepres.findViewById(R.id.fa_type)).setText(R.string.NDFA);
            ((TextView) faRepres.findViewById(R.id.fa_type)).setBackgroundResource(R.drawable.ic_undetermined_automaton_black);
        } else {
            //((TextView) faRepres.findViewById(R.id.fa_type)).setText(R.string.FA);
            ((TextView) faRepres.findViewById(R.id.fa_type)).setBackgroundResource(R.drawable.ic_determined_automaton_black);

        }
        tv.setText(name);
        faRepres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (currentState) {
                    case OPEN:
                    {
                        Intent faCreatorIntent = new Intent(ma.getApplicationContext(), AutomatonActivity.class);
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable("fa_table", table);
                        faCreatorIntent.putExtra("fa_id", id);
                        faCreatorIntent.putExtra("fa_name", name);
                        faCreatorIntent.putExtra("fa_desc", desc);
                        faCreatorIntent.putExtra("is_ndfa", isNDFA);
                        faCreatorIntent.putExtras(mBundle);
                        startActivityForResult(faCreatorIntent, 108);
                        break;
                    }
                    case INTERSECTION:
                        bar.dismiss();
                        automatonOperator.addIntersectionOfAutomata(currentFA, id);
                        updateFAList();
                        currentState = OPEN;
                        break;
                    case COMPARATION:
                        bar.dismiss();
                        boolean equals = automatonOperator.compareAutomata(currentFA, id);
                        if (equals) {
                            Toast.makeText(ma, getResources().getString(R.string.result_equal), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ma, getResources().getString(R.string.result_diff), Toast.LENGTH_SHORT).show();
                        }
                        currentState = OPEN;
                        break;
                }
            }
        });
        registerForContextMenu(faRepres);
        automataGrid.addView(faRepres);
    }

    public void updateFAList() {
        automataGrid.removeAllViews();
        automataIDs.clear();
        List<FiniteAutomaton> automata = automatonRequisitor.requireAutomata();
        for (FiniteAutomaton a : automata) {
            createFAItem(a.getName(), a.getDesc(), a.isNdfa(), a.getId(), a.generateArrayRepresentationOfTransitions());
        }
        if (automata.isEmpty()) {
            rootView.findViewById(R.id.no_fa_placeholder).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int index = automataGrid.indexOfChild(v);
        currentFA = automataIDs.get(index);
        currentFAName = ((TextView) v.findViewById(R.id.fa_label)).getText().toString();
        String currentType = ((TextView) v.findViewById(R.id.fa_type)).getText().toString();
        menu.setHeaderTitle(currentFAName);
        menu.add(0, v.getId(), 0, R.string.delete_fa);
        menu.add(0, v.getId(), 0, R.string.put_word);
        menu.add(0, v.getId(), 0, R.string.words_n_that_fa_recognize);
        if (automatonOperator.isAutomatonNDFA(currentFA)) {
            menu.add(0, v.getId(), 0, R.string.determinize);
        }
        menu.add(0, v.getId(), 0, R.string.compare);
        menu.add(0, v.getId(), 0, R.string.intersec);
        menu.add(0, v.getId(), 0, R.string.minimize);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(((ViewPager)ma.findViewById(R.id.viewpager)).getCurrentItem() != 0) {
            return false;
        }
        if (item.getTitle().equals(getResources().getString(R.string.delete_fa))) {
            new AlertDialog.Builder(ma)
                    .setTitle(getResources().getString(R.string.del) + "\"" + currentFAName + "\"")
                    .setMessage(R.string.ru_sure_del_automaton)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            automatonOperator.delAutomaton(currentFA);
                            updateFAList();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
            return true;
        } else if (item.getTitle() == getResources().getString(R.string.put_word)) {
            final EditText et = new EditText(ma);
            new AlertDialog.Builder(ma)
                    .setTitle("Testar \"" + currentFAName + "\"")
                    .setMessage("insira a palavra a submeter ao autômato")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setView(et)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (automatonOperator.testFA(currentFA, et.getText().toString())) {
                                Toast.makeText(ma, "palavra pertence", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(ma, "palavra n pertence", Toast.LENGTH_SHORT).show();
                            }

                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
            return true;
        } else if (item.getTitle().equals(getResources().getString(R.string.words_n_that_fa_recognize))) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ma);
            builder.setTitle(getResources().getString(R.string.type_n));
            final EditText input = new EditText(ma);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            builder.setView(input);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String nT = input.getText().toString();
                    int n = Integer.parseInt(nT);
                    List<String> sentences = automatonOperator.sentenceThatFARecognize(currentFA, n);
                    SentenceList list = new SentenceList(ma);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ma);
                    builder.setTitle(getResources().getString(R.string.sentence_n_list));
                    list.build(sentences);
                    builder.setView(list.getLayout());
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        } else if (item.getTitle().equals(getResources().getString(R.string.determinize))) {
            automatonOperator.determinizeAndAddAutomata(currentFA);
            updateFAList();
        } else if (item.getTitle().equals(getResources().getString(R.string.intersec))) {
            bar = Snackbar.make(ma.findViewById(R.id.main), getResources().getString(R.string.choose_to_intersection), Snackbar.LENGTH_INDEFINITE);
            bar.show();
            currentState = INTERSECTION;
        } else if (item.getTitle().equals(getResources().getString(R.string.compare))) {
            bar = Snackbar.make(ma.findViewById(R.id.main), getResources().getString(R.string.choose_to_compare), Snackbar.LENGTH_INDEFINITE);
            bar.show();
            currentState = COMPARATION;
        } else if (item.getTitle().equals(getResources().getString(R.string.minimize))) {
            automatonOperator.minimizeAndAddAutomaton(currentFA);
            updateFAList();
        }

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 108) {
            updateFAList();
        }
    }

    private View inflateView(int xmlCode, View parent) {
        LayoutInflater inflater = (LayoutInflater) ma.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(xmlCode, (ViewGroup)parent, false);
    }

    public void addNewElement(View view) {
        Intent faCreatorIntent = new Intent(ma.getApplicationContext(), AutomatonActivity.class);
        Bundle mBundle = new Bundle();
        String[][] stubTable = {{getResources().getString(R.string.delta), ""}, {"", ""}};
        mBundle.putSerializable("fa_table", stubTable);
        faCreatorIntent.putExtra("fa_id", "");
        faCreatorIntent.putExtra("fa_name", getString(R.string.new_fa));
        faCreatorIntent.putExtra("fa_desc", "");
        faCreatorIntent.putExtra("is_ndfa", false);
        faCreatorIntent.putExtras(mBundle);
        startActivityForResult(faCreatorIntent, 108);
    }
}