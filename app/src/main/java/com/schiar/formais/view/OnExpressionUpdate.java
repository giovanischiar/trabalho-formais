package com.schiar.formais.view;

/**
 * Created by Giovani on 14/11/2016.
 */
public interface OnExpressionUpdate {
    void updateExpression(String expression);
}
