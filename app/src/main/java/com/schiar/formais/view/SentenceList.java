package com.schiar.formais.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.schiar.formais.R;

import java.util.List;

/**
 * Created by Giovani on 07/11/2016.
 */

public class SentenceList {
    private Context context;
    private LinearLayout layout;
    private ScrollView parentLayout;

    public SentenceList(Context context) {
        this.context = context;
        parentLayout = (ScrollView) inflateView(R.layout.view_sentence_list, null);
        layout = (LinearLayout)parentLayout.findViewById(R.id.sentence_ul);
    }

    public void build(List<String> sentences) {
        layout.removeAllViews();
        for(String sentence : sentences) {
            TextView sentenceEntry = (TextView) inflateView(R.layout.view_sentence_entry, layout);
            sentenceEntry.setText(sentence);
            layout.addView(sentenceEntry);
        }
    }

    public View getLayout() {
        return parentLayout;
    }

    private View inflateView(int xmlCode, View parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(xmlCode, (ViewGroup)parent, false);
    }
}
