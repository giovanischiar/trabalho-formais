package com.schiar.formais.view;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;

import com.schiar.formais.R;

/**
 * Created by Giovani on 18/10/2016.
 */
public class AutomatonAttEditor {
    private TextInputLayout editNameLayout;
    private TextInputLayout editDescLayout;

    private CheckBox checkBoxIsNDFA;
    private OnNameChanger onNameChanger;
    private OnCheckBoxChange onCheckBoxChange;

    public AutomatonAttEditor(Context context, String name, String desc, boolean isNDFA) {
        AutomatonActivity automatonActivity = ((AutomatonActivity) context);

        editNameLayout = (TextInputLayout) automatonActivity.findViewById(R.id.edit_name);
        editDescLayout = (TextInputLayout) automatonActivity.findViewById(R.id.edit_desc);


        checkBoxIsNDFA = (CheckBox)automatonActivity.findViewById(R.id.is_ndfa);
        onNameChanger = (OnNameChanger) context;
        onCheckBoxChange = (OnCheckBoxChange) context;
        setAtt(name, desc, isNDFA);
    }

    public void beginEdition() {
        editNameLayout.setVisibility(View.VISIBLE);
        editDescLayout.setVisibility(View.VISIBLE);
        checkBoxIsNDFA.setVisibility(View.VISIBLE);
    }

    public void finishEdition() {
        editNameLayout.setVisibility(View.GONE);
        editDescLayout.setVisibility(View.GONE);
        checkBoxIsNDFA.setVisibility(View.GONE);
    }

    public void build() {
        editNameLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("magnamite", "text change!");
                onNameChanger.updateTitle(AutomatonAttEditor.this.getName());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editDescLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("magnamite", "text change!");
                onNameChanger.updateDesc(AutomatonAttEditor.this.getDesc());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        checkBoxIsNDFA.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                onCheckBoxChange.checkboxChanged();
            }

            @Override
            public void onViewDetachedFromWindow(View v) {

            }
        });
    }

    public void setAtt(String name, String desc, boolean isNDFA) {
        editNameLayout.getEditText().setText(name);
        editDescLayout.getEditText().setText(desc);
        checkBoxIsNDFA.setChecked(isNDFA);
    }

    public String getName() {
        return editNameLayout.getEditText().getText().toString();
    }

    public String getDesc() {
        return editDescLayout.getEditText().getText().toString();
    }

    public boolean isNDFA() {
        return checkBoxIsNDFA.isChecked();
    }
}
