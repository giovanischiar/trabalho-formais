package com.schiar.formais.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.schiar.formais.Formais;
import com.schiar.formais.R;
import com.schiar.formais.controller.RegexSaver;

import java.util.UUID;

public class RegexActivity extends AppCompatActivity implements OnNameChanger, OnExpressionUpdate {

    private class DataFromIntent {
        String id;
        String name;
        String desc;
        String expression;
    }

    private Menu menu;
    private RegexAttEditor regexAttEditor;
    private boolean editMode = false;
    private RegexSaver regexSaver;
    private FloatingActionButton fab;
    private DataFromIntent dataFromIntent = new DataFromIntent();
    private boolean creating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regex);
        setupActionBar();
        creating = false;
        Formais formais = (Formais)getApplication();
        regexSaver = formais.getRegexSaver();
        setupDataFromIntent();
        updateTitle(dataFromIntent.name);
        updateDesc(dataFromIntent.desc);
        updateExpression(dataFromIntent.expression);
    }

    public void updateExpression(String expression) {
        ((TextView)findViewById(R.id.expression)).setText(expression);
    }

    private void setupDataFromIntent() {
        dataFromIntent.name = getIntent().getStringExtra("regex_name");
        dataFromIntent.desc = getIntent().getStringExtra("regex_desc");
        dataFromIntent.id = getIntent().getStringExtra("regex_id");
        dataFromIntent.expression = getIntent().getStringExtra("regex_expression");
    }

    private void setupViewComponents() {
        fab = (FloatingActionButton) findViewById(R.id.fab_save);
        regexAttEditor = new RegexAttEditor(this, dataFromIntent.name, dataFromIntent.desc, dataFromIntent.expression);
        regexAttEditor.build();
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    public void updateTitle(String name) {
        //final String title = getString(R.string.fa);
        setTitle(name);
    }

    @Override
    public void updateDesc(String desc) {
        ((TextView)findViewById(R.id.desc)).setText(desc);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fa_creator_menu_options, menu);
        this.menu = menu;
        setupViewComponents();
        if(dataFromIntent.id.isEmpty()) {
            creating = true;
            turnOnEditMode();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.done:
                String oldId = dataFromIntent.id;
                String name = regexAttEditor.getName();
                String desc = regexAttEditor.getDesc();
                String expression = regexAttEditor.getExpression();
                String id;
                if(dataChanged()) {
                    id = UUID.randomUUID().toString();
                } else {
                    id = dataFromIntent.id;
                }
                boolean added = regexSaver.addRegex(oldId, id, name, desc, expression);
                if(!added) {
                    Toast.makeText(this, getString(R.string.errors_on_regex), Toast.LENGTH_SHORT).show();
                } else {
                    dataFromIntent.id = id;
                    Toast.makeText(this, getString(R.string.regex_saved), Toast.LENGTH_SHORT).show();
                }
                turnOffEditMode();
                return true;

            case android.R.id.home:
                confirmAndCancelAutomatonEditing();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void confirmAndCancelAutomatonEditing() {
        if(creating && !dataChanged()) {
            cancelAutomataEdit();
            return;
        }
        if(editMode) {
            if(dataChanged()) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.cancel_regex)
                        .setMessage(R.string.confirm_cancel_regex)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.discard, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                undoEdit();
                            }})
                        .setNegativeButton(R.string.keep_editing, null).show();
            } else {
                undoEdit();
            }
        } else {
            cancelAutomataEdit();
        }
    }

    public void undoEdit() {
        regexAttEditor.setAtt(dataFromIntent.name, dataFromIntent.desc, dataFromIntent.expression);
        turnOffEditMode();
    }

    public void turnOffEditMode() {
        menu.findItem(R.id.done).setVisible(false);
        regexAttEditor.finishEdition();
        findViewById(R.id.desc).setVisibility(View.VISIBLE);
        findViewById(R.id.expression).setVisibility(View.VISIBLE);
        fab.setVisibility(View.VISIBLE);
        editMode = false;
    }

    public void turnOnEditMode() {
        menu.findItem(R.id.done).setVisible(true);
        regexAttEditor.beginEdition();
        findViewById(R.id.desc).setVisibility(View.GONE);
        findViewById(R.id.expression).setVisibility(View.GONE);
        fab.setVisibility(View.GONE);
        menu.findItem(R.id.done).setVisible(true);
        editMode = true;
    }

    public void editAutomaton(View v) {
        turnOnEditMode();
    }

    public boolean dataChanged() {
        String newName = regexAttEditor.getName();
        String newDesc = regexAttEditor.getDesc();
        String newExpression = regexAttEditor.getExpression();
        String oldName = dataFromIntent.name;
        String oldDesc = dataFromIntent.desc;
        String oldExpression = dataFromIntent.expression;
        return (!oldName.equals(newName) || !oldDesc.equals(newDesc) ||
                !oldExpression.equals(newExpression));
    }

    @Override
    public void onBackPressed() {
        confirmAndCancelAutomatonEditing();
    }

    public void cancelAutomataEdit() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void appendTextToExpression(View v) {
        regexAttEditor.appendStringToExpression(((Button)v).getText().toString());
    }
}
