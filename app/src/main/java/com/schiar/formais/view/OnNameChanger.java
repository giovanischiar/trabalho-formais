package com.schiar.formais.view;

/**
 * Created by Giovani on 18/10/2016.
 */
public interface OnNameChanger {
    void updateTitle(String name);
    void updateDesc(String desc);
}
