package com.schiar.formais.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.schiar.formais.Formais;
import com.schiar.formais.R;
import com.schiar.formais.controller.RegexOperator;
import com.schiar.formais.controller.RegexRequisitor;
import com.schiar.formaisLib.model.regex.Regex;

import java.util.ArrayList;
import java.util.List;

import static com.schiar.formais.view.AutomataFragment.IconState.COMPARATION;
import static com.schiar.formais.view.AutomataFragment.IconState.INTERSECTION;
import static com.schiar.formais.view.AutomataFragment.IconState.OPEN;

public class RegicesFragment extends Fragment {

    private RegexOperator regexOperator;
    private RegexRequisitor regexRequisitor;
    private String regexIdSelected, regexNameSelected;
    public enum IconState {OPEN, INTERSECTION, COMPARATION};
    private AutomataFragment.IconState currentState;
    private List<String> regicesIDs;
    private Snackbar bar;
    private MainActivity ma;
    private View rootView;
    private LinearLayout regexLayout;
    private boolean activityCreated;
    private boolean rootViewCreated;

    public RegicesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityCreated = false;
        rootViewCreated = false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ma = (MainActivity) getActivity();
        regicesIDs = new ArrayList<>();
        currentState = OPEN;
        regexOperator = ((Formais)ma.getApplication()).getRegexOperator();
        regexRequisitor = ((Formais)ma.getApplication()).getRegexRequisitor();
        bar = Snackbar.make(ma.findViewById(R.id.main), "", Snackbar.LENGTH_INDEFINITE);
        activityCreated = true;
        if(rootViewCreated) {
            updateRegexList();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_regex, container, false);
        regexLayout = (LinearLayout) rootView.findViewById(R.id.regexLayout);
        rootViewCreated = true;
        if(activityCreated) {
            updateRegexList();
        }
        return rootView;
    }

    public void createRegexItem(final String name, final String desc, final String id, final String expression) {
        regicesIDs.add(id);
        rootView.findViewById(R.id.no_fa_placeholder).setVisibility(View.GONE);
        regexLayout.setVisibility(View.VISIBLE);
        RelativeLayout regexIcon = (RelativeLayout) inflateView(R.layout.view_regex, regexLayout);
        TextView tv = (TextView) regexIcon.findViewById(R.id.regex_label);
        ((TextView) regexIcon.findViewById(R.id.regex)).setText(expression);

        tv.setText(name);
        regexIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (currentState) {
                    case OPEN:
                    {
                        Intent faCreatorIntent = new Intent(ma.getApplicationContext(), RegexActivity.class);
                        faCreatorIntent.putExtra("regex_id", id);
                        faCreatorIntent.putExtra("regex_name", name);
                        faCreatorIntent.putExtra("regex_desc", desc);
                        faCreatorIntent.putExtra("regex_expression", expression);
                        startActivityForResult(faCreatorIntent, 108);
                        break;
                    }
                    case INTERSECTION:
//                        bar.dismiss();
//                        automatonOperator.addIntersectionOfAutomata(currentFA, id);
//                        updateRegexList();
//                        currentState = OPEN;
                        break;
                    case COMPARATION:
                        bar.dismiss();
//                        boolean equals = automatonOperator.compareAutomata(currentFA, id);
//                        if (equals) {
//                            Toast.makeText(ma, getResources().getString(R.string.result_equal), Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(ma, getResources().getString(R.string.result_diff), Toast.LENGTH_SHORT).show();
//                        }
//                        currentState = OPEN;
                        break;
                }
            }
        });
        registerForContextMenu(regexIcon);
        regexLayout.addView(regexIcon);
    }

    public void updateRegexList() {
        regexLayout.removeAllViews();
        regicesIDs.clear();
        List<Regex> regices = regexRequisitor.requireRegices();
        for (Regex regex : regices) {
            createRegexItem(regex.getName(), regex.getDescription(), regex.getId(), regex.getExpression());
        }
        if (regices.isEmpty()) {
            rootView.findViewById(R.id.no_fa_placeholder).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        int index = regexLayout.indexOfChild(v);
        regexIdSelected = regicesIDs.get(index);
        regexNameSelected = ((TextView) v.findViewById(R.id.regex_label)).getText().toString();
        menu.setHeaderTitle(regexNameSelected);
        menu.add(0, v.getId(), 0, R.string.delete_fa);
        menu.add(0, v.getId(), 0, R.string.convert_to_automata);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(((ViewPager)ma.findViewById(R.id.viewpager)).getCurrentItem() != 1) {
            return false;
        }
        if (item.getTitle().equals(getResources().getString(R.string.delete_fa))) {
            new AlertDialog.Builder(ma)
                    .setTitle(getResources().getString(R.string.del) + "\"" + regexNameSelected + "\"")
                    .setMessage(R.string.ru_sure_del_regex)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            regexOperator.delRegex(regexIdSelected);
                            updateRegexList();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
            return true;
        } else if (item.getTitle().equals(getResources().getString(R.string.intersec))) {
            bar = Snackbar.make(ma.findViewById(R.id.main), getResources().getString(R.string.choose_to_intersection), Snackbar.LENGTH_INDEFINITE);
            bar.show();
            currentState = INTERSECTION;
        } else if (item.getTitle().equals(getResources().getString(R.string.compare))) {
            bar = Snackbar.make(ma.findViewById(R.id.main), getResources().getString(R.string.choose_to_compare), Snackbar.LENGTH_INDEFINITE);
            bar.show();
            currentState = COMPARATION;
        } else if (item.getTitle().equals(getResources().getString(R.string.convert_to_automata))) {
            regexOperator.convertToAutomata(regexIdSelected);
            ma.switchFragment();
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 108) {
            updateRegexList();
        }
    }

    private View inflateView(int xmlCode, View parent) {
        LayoutInflater inflater = (LayoutInflater) ma.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(xmlCode, (ViewGroup)parent, false);
    }

    public void addNewElement(View view) {
        Intent faCreatorIntent = new Intent(ma.getApplicationContext(), RegexActivity.class);
        faCreatorIntent.putExtra("regex_id", "");
        faCreatorIntent.putExtra("regex_name", getString(R.string.new_regex));
        faCreatorIntent.putExtra("regex_desc", "");
        faCreatorIntent.putExtra("regex_expression", "");
        startActivityForResult(faCreatorIntent, 108);
    }
 
}