package com.schiar.formais.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.schiar.formais.Formais;
import com.schiar.formais.R;
import com.schiar.formais.controller.AutomatonSaver;
import com.schiar.formais.controller.TableValidator;

import java.util.Arrays;
import java.util.UUID;

public class AutomatonActivity extends AppCompatActivity implements OnNameChanger, OnCheckBoxChange {

    private class DataFromIntent {
        String id;
        String name;
        String desc;
        boolean ndfa;
        String[][] transitionTable;
    }

    private Menu menu;
    private Table table;
    private TableScaleButton tableScaleButton;
    private AutomatonAttEditor automatonAttEditor;
    private boolean editMode = false;
    private TableValidator tableValidate;
    private AutomatonSaver automatonSaver;
    private FloatingActionButton fab;
    private DataFromIntent dataFromIntent = new DataFromIntent();
    private boolean creating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_automaton);
        setupActionBar();
        Formais formais = (Formais)getApplication();
        tableValidate = formais.getTableValidator();
        automatonSaver = formais.getAutomatonSaver();
        setupDataFromIntent();
        updateTitle(dataFromIntent.name);
        updateDesc(dataFromIntent.desc);
        creating = false;
    }

    private void setupDataFromIntent() {
        dataFromIntent.name = getIntent().getStringExtra("fa_name");
        dataFromIntent.desc = getIntent().getStringExtra("fa_desc");
        dataFromIntent.id = getIntent().getStringExtra("fa_id");
        dataFromIntent.ndfa = getIntent().getBooleanExtra("is_ndfa", false);
        dataFromIntent.transitionTable = (String[][])getIntent().getSerializableExtra("fa_table");
    }

    private void setupViewComponents() {
        fab = (FloatingActionButton) findViewById(R.id.fab_save);
        automatonAttEditor = new AutomatonAttEditor(this, dataFromIntent.name, dataFromIntent.desc, dataFromIntent.ndfa);
        table = new Table(this, dataFromIntent.transitionTable);
        tableScaleButton = new TableScaleButton(this, table);
        automatonAttEditor.build();
        table.build();
        tableScaleButton.build();
        tableChanged();
    }

    private void setupActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
        }
    }

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    public void updateTitle(String name) {
        //final String title = getString(R.string.fa);
        setTitle(name);
    }

    @Override
    public void updateDesc(String desc) {
        ((TextView)findViewById(R.id.desc)).setText(desc);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fa_creator_menu_options, menu);
        this.menu = menu;
        setupViewComponents();
        if(dataFromIntent.id.isEmpty()) {
            creating = true;
            turnOnEditMode();
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.done:
                String oldId = dataFromIntent.id;
                String name = automatonAttEditor.getName();
                String desc = automatonAttEditor.getDesc();
                boolean isNDFA = automatonAttEditor.isNDFA();
                String[][] tableArray = table.getTableData();
                String id;
                if(dataChanged()) {
                    id = UUID.randomUUID().toString();
                } else {
                    id = dataFromIntent.id;
                }
                boolean added = automatonSaver.addAutomata(oldId, id, name, desc, isNDFA, tableArray);
                if(!added) {
                    Toast.makeText(this, getString(R.string.errors_on_fa), Toast.LENGTH_SHORT).show();
                } else {
                    dataFromIntent.id = id;
                    Toast.makeText(this, getString(R.string.automaton_saved), Toast.LENGTH_SHORT).show();
                }
                turnOffEditMode();
                return true;

            case android.R.id.home:
                confirmAndCancelAutomatonEditing();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void confirmAndCancelAutomatonEditing() {
        if(creating && !dataChanged()) {
            cancelAutomataEdit();
            return;
        }
        if(editMode) {
            if(dataChanged()) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.cancel_automaton)
                        .setMessage(R.string.confirm_cancel_automaton)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.discard, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                undoEdit();
                            }})
                        .setNegativeButton(R.string.keep_editing, null).show();
            } else {
                undoEdit();
            }
        } else {
            cancelAutomataEdit();
        }
    }

    public void undoEdit() {
        automatonAttEditor.setAtt(dataFromIntent.name, dataFromIntent.desc, dataFromIntent.ndfa);
        table.rebuild();
        tableScaleButton.update();
        turnOffEditMode();
    }

    public void turnOffEditMode() {
        menu.findItem(R.id.done).setVisible(false);
        automatonAttEditor.finishEdition();
        findViewById(R.id.desc).setVisibility(View.VISIBLE);
        tableScaleButton.vanish();
        fab.setVisibility(View.VISIBLE);
        table.disable();
        editMode = false;
    }

    public void turnOnEditMode() {
        tableScaleButton.appear();
        menu.findItem(R.id.done).setVisible(true);
        automatonAttEditor.beginEdition();
        findViewById(R.id.desc).setVisibility(View.GONE);
        fab.setVisibility(View.GONE);
        menu.findItem(R.id.done).setVisible(true);
        table.enable();
        editMode = true;

    }

    public void editAutomaton(View v) {
        turnOnEditMode();
    }

    public boolean dataChanged() {
        String newName = automatonAttEditor.getName();
        String newDesc = automatonAttEditor.getDesc();
        boolean newIsNdfa = automatonAttEditor.isNDFA();
        String[][] newTable = table.getTableData();
        String oldName = dataFromIntent.name;
        String oldDesc = dataFromIntent.desc;
        boolean oldIsNdfa = dataFromIntent.ndfa;
        String[][] oldTable = dataFromIntent.transitionTable;
        return (!oldName.equals(newName) || !oldDesc.equals(newDesc) ||
                oldIsNdfa != newIsNdfa || !Arrays.deepEquals(oldTable, newTable));
    }

    @Override
    public void onBackPressed() {
        confirmAndCancelAutomatonEditing();
    }

    public void cancelAutomataEdit() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void tableChanged() {
        if(editMode) {
            String[][] tableFromData = table.getTableData();
            boolean isValid = tableValidate.isTableValid(tableFromData, automatonAttEditor.isNDFA());
            if(isValid) {
                menu.findItem(R.id.done).setVisible(true);
            } else {
                menu.findItem(R.id.done).setVisible(false);
            }
        }
    }

    public void checkboxChanged() {
        tableChanged();
    }
}
