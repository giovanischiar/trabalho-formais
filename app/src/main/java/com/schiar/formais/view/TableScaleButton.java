package com.schiar.formais.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.schiar.formais.R;

/**
 * Created by Giovani on 18/10/2016.
 */
public class TableScaleButton {

    int cellHeight, cellWidth;
    private Context context;
    private RelativeLayout layout;
    private Table table;
    private Button changer;

    public TableScaleButton(Context context, Table table) {
        layout = (RelativeLayout)((Activity) context).findViewById(R.id.relLayout);
        this.context = context;
        this.table = table;
        cellHeight = (int)context.getResources().getDimension(R.dimen.fa_cell_height);
        cellWidth = (int)context.getResources().getDimension(R.dimen.fa_cell_width);
    }

    public void build() {
        changer = inflateButton(R.layout.view_table_scaler_button, layout);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams)changer.getLayoutParams();
        int faEditorHeight = (int) context.getResources().getDimension(R.dimen.fa_editor_height);
        int leftPosition = cellWidth*table.getColumnQTD();
        int topPosition = cellHeight*table.getRowQTD();
        lp.setMargins(leftPosition, topPosition, 0, 0);
        changer.setLayoutParams(lp);
        changer.setOnTouchListener(new View.OnTouchListener() {
            float x1 = 0;
            float x2 = 0;
            float y1 = 0;
            float y2 = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final HorizontalScrollView scrollView = (HorizontalScrollView) v.getParent().getParent();
                scrollView.requestDisallowInterceptTouchEvent(true);

                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        y1 = event.getY();
                        changer.setBackground(ContextCompat.getDrawable(context, R.drawable.changer_border_pressed));
                        break;

                    case MotionEvent.ACTION_UP:
                        changer.setBackground(ContextCompat.getDrawable(context, R.drawable.changer_border));
                        break;

                    case MotionEvent.ACTION_MOVE: {
                        x2 = event.getX();
                        y2 = event.getY();

                        if((x1 < x2 && (x2-x1) > cellWidth) && (y1 < y2 && (y2 - y1) > cellHeight)) {
                            table.changeTable(Table.Modification.ADD_ROW_COLUMN);
                            update();
                        }

                        if((x1 > x2 && (x1-x2) > cellWidth)&&(y1 > y2 && (y1 - y2) > cellHeight)) {
                            table.changeTable(Table.Modification.REM_ROW_COLUMN);
                            update();
                        }

                        if(x1 < x2 && (x2-x1) > cellWidth) {
                            Log.d("magnamite", "adding column");
                            table.changeTable(Table.Modification.ADD_COLUMN);
                            update();
                        }

                        if(x1 > x2 && (x1-x2) > cellWidth) {
                            table.changeTable(Table.Modification.REM_COLUMN);
                            update();
                        }

                        if (y1 < y2 && (y2 - y1) > cellHeight) {
                            Log.d("magnamite", "adding row");
                            table.changeTable(Table.Modification.ADD_ROW);
                            update();
                        }

                        if (y1 > y2 && (y1 - y2) > cellHeight) {
                            table.changeTable(Table.Modification.REM_ROW);
                            update();
                        }
                    }
                    break;
                }
                return false;
            }
        });
        layout.addView(changer);
    }

    public void update() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams)changer.getLayoutParams();
        int faEditorHeight = (int) context.getResources().getDimension(R.dimen.fa_editor_height);
        int leftPosition = cellWidth*table.getColumnQTD();
        int topPosition = cellHeight*table.getRowQTD();
        lp.setMargins(leftPosition, topPosition, 0, 0);
        changer.setLayoutParams(lp);
    }

    private Button inflateButton(int xmlCode, View parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return (Button)inflater.inflate(xmlCode, (ViewGroup)parent, false);
    }

    public void vanish() {
        changer.setVisibility(View.GONE);
    }

    public void appear() {
        changer.setVisibility(View.VISIBLE);
    }
}
