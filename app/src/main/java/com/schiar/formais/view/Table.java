package com.schiar.formais.view;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.schiar.formais.R;

/**
 * Created by Giovani on 18/10/2016.
 */
public class Table {
    public enum Modification {ADD_ROW, ADD_COLUMN, REM_ROW, REM_COLUMN, ADD_ROW_COLUMN, REM_ROW_COLUMN};
    private int columnQTD;
    private int rowQTD;
    private String[][]  transitionTable;
    private boolean enable;
    private Context context;
    private LinearLayout layout;

    public Table(Context context, int columnQTD, int rowQTD) {
        layout = (LinearLayout) inflateView(R.id.table, null);
        this.context = context;
        this.columnQTD = columnQTD;
        this.rowQTD = rowQTD;
    }

    public Table(Context context, String[][] transitionTable) {
        layout = (LinearLayout) ((Activity) context).findViewById(R.id.table);
        this.context = context;
        enable = false;
        columnQTD = transitionTable[0].length;
        rowQTD = transitionTable.length;
        this.transitionTable = transitionTable;
    }

    public void rebuild() {
        layout.removeAllViews();
        columnQTD = transitionTable[0].length;
        rowQTD = transitionTable.length;
        build();
    }

    public void build() {
        for(int i = 0; i < columnQTD; i++) {
            LinearLayout column = new LinearLayout(context);
            column.setOrientation(LinearLayout.VERTICAL);
            for(int j = 0; j < rowQTD; j++) {
                if(i+j == 0) {
                    TextView tv = (TextView)inflateView(R.layout.view_table_cell, column);
                    column.addView(tv);
                    continue;
                }
                EditText text = createTableEditCell(column);
                if(j == 0) {
                    text.setFilters(new InputFilter[] {new InputFilter.LengthFilter(1)});
                }
                column.addView(text);
            }
            layout.addView(column);
        }
        fillTable();
    }

    private void fillTable() {
        for(int i = 0; i < rowQTD; i++) {
            for(int j = 0; j < columnQTD; j++) {
                if(i+j == 0) continue;
                LinearLayout column = (LinearLayout)layout.getChildAt(j);
                EditText editText = (EditText) column.getChildAt(i);
                String[] row = transitionTable[i];
                String element = row[j];
                editText.setText(element);
            }
        }
    }

    private View inflateView(int xmlCode, View parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(xmlCode, (ViewGroup)parent, false);
    }

    public void changeTable(Modification modification) {
        switch (modification) {
            case ADD_ROW:
                addRow();
                break;
            case ADD_COLUMN:
                addColumn();
                break;
            case REM_ROW:
                remRow();
                break;
            case REM_COLUMN:
                remColumn();
                break;
            case ADD_ROW_COLUMN:
                addRow();
                addColumn();
                break;
            case REM_ROW_COLUMN:
                remRow();
                remColumn();
                break;
        }
        ((AutomatonActivity) context).tableChanged();
    }

    private void addRow() {
        for(int i = 0; i < layout.getChildCount(); i++) {
            LinearLayout column = (LinearLayout)layout.getChildAt(i);
            EditText text = createTableEditCell(column);
            text.setEnabled(enable);
            column.addView(text);

        }
        rowQTD++;
    }

    private void remRow() {
        if(((LinearLayout)layout.getChildAt(0)).getChildCount() == 2) {
            return;
        }
        for(int i = 0; i < layout.getChildCount(); i++) {
            LinearLayout row = ((LinearLayout)layout.getChildAt(i));
            int cellQTD = row.getChildCount();
            View lastView;
            lastView = row.getChildAt(cellQTD-1);
            row.removeView(lastView);
            layout.invalidate();
        }
        rowQTD--;
    }

    private void addColumn() {
        LinearLayout row = new LinearLayout(context);
        row.setOrientation(LinearLayout.VERTICAL);
        int columnCount = ((LinearLayout)layout.getChildAt(0)).getChildCount();
        for(int i = 0; i < columnCount; i++) {
            EditText text = createTableEditCell(row);
            if(i == 0) {
                text.setFilters(new InputFilter[] {new InputFilter.LengthFilter(1)});
            }
            text.setEnabled(enable);
            row.addView(text);
        }
        int a = layout.getChildCount();
        layout.addView(row, a);

        columnQTD++;
    }

    private void remColumn() {
        if(layout.getChildCount() == 2) {
            return;
        }
        int lastChildIndex = layout.getChildCount()-1;
        LinearLayout lastChild = (LinearLayout) layout.getChildAt(lastChildIndex);
        layout.removeView(lastChild);
        columnQTD--;
    }

    public void enable() {
        enable = true;
        for(int i = 0; i < layout.getChildCount(); i++) {
            for(int j = 0;j < ((LinearLayout)layout.getChildAt(i)).getChildCount(); j++) {
                ((LinearLayout)layout.getChildAt(i)).getChildAt(j).setEnabled(true);
            }
        }
    }

    public void disable() {
        enable = false;
        for(int i = 0; i < layout.getChildCount(); i++) {
            for(int j = 0;j < ((LinearLayout)layout.getChildAt(i)).getChildCount(); j++) {
                ((LinearLayout)layout.getChildAt(i)).getChildAt(j).setEnabled(false);
            }
        }
    }

    public int getColumnQTD() {
        return columnQTD;
    }

    public int getRowQTD() {
        return rowQTD;
    }

    public String[][] getTableData() {
        String[][] table = new String[rowQTD][columnQTD];
        for(int i = 0; i < columnQTD; i++) {
            for(int j = 0; j < rowQTD; j++) {
                if(i+j == 0) {table[0][0] = "δ"; continue;}
                LinearLayout column = (LinearLayout)layout.getChildAt(i);
                EditText editText = (EditText) column.getChildAt(j);
                table[j][i] = editText.getText().toString();
            }
        }

        return table;
    }

    public EditText createTableEditCell(View column) {
        EditText editText = (EditText) inflateView(R.layout.view_editable_table_cell, column);
        setOnChangeListenersOnView(editText);
        return editText;
    }

    public void setOnChangeListenersOnView(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ((AutomatonActivity) context).tableChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
