package com.schiar.formais.view;

/**
 * Created by Giovani on 17/11/2016.
 */

public interface OnCheckBoxChange {
    void checkboxChanged();
}
