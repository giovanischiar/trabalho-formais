package com.schiar.formais.view;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.schiar.formais.R;

/**
 * Created by Giovani on 18/10/2016.
 */
public class RegexAttEditor {
    private TextInputLayout editNameLayout;
    private TextInputLayout editDescLayout;
    private TextInputLayout editExpressionLayout;
    private LinearLayout expressionEditor;

    private OnNameChanger onNameChanger;
    private OnExpressionUpdate onExpressionUpdate;

    public RegexAttEditor(Context context, String name, String desc, String expression) {
        Activity automatonActivity = ((Activity) context);

        editNameLayout = (TextInputLayout) automatonActivity.findViewById(R.id.edit_name);
        editDescLayout = (TextInputLayout) automatonActivity.findViewById(R.id.edit_desc);
        expressionEditor = (LinearLayout) automatonActivity.findViewById(R.id.expressionEditor);
        editExpressionLayout = (TextInputLayout) automatonActivity.findViewById(R.id.edit_expression);


        this.onNameChanger = (OnNameChanger) context;
        this.onExpressionUpdate = (OnExpressionUpdate) context;
        setAtt(name, desc, expression);
    }

    public void beginEdition() {
        editNameLayout.setVisibility(View.VISIBLE);
        editDescLayout.setVisibility(View.VISIBLE);
        expressionEditor.setVisibility(View.VISIBLE);
    }

    public void finishEdition() {
        editNameLayout.setVisibility(View.GONE);
        editDescLayout.setVisibility(View.GONE);
        expressionEditor.setVisibility(View.GONE);
    }

    public void build() {
        editNameLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onNameChanger.updateTitle(RegexAttEditor.this.getName());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editDescLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onNameChanger.updateDesc(RegexAttEditor.this.getDesc());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editExpressionLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onExpressionUpdate.updateExpression(RegexAttEditor.this.getExpression());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setAtt(String name, String desc, String expression) {
        editNameLayout.getEditText().setText(name);
        editDescLayout.getEditText().setText(desc);
        editExpressionLayout.getEditText().setText(expression);
    }

    public String getName() {
        return editNameLayout.getEditText().getText().toString();
    }

    public String getDesc() {
        return editDescLayout.getEditText().getText().toString();
    }

    public String getExpression() {
        return editExpressionLayout.getEditText().getText().toString();
    }

    public void appendStringToExpression(String c) {
        EditText editText = editExpressionLayout.getEditText();
        if(editText != null) {
            int start = Math.max(editText.getSelectionStart(), 0);
            int end = Math.max(editText.getSelectionEnd(), 0);
            editText.getText().replace(Math.min(start, end), Math.max(start, end), c, 0, c.length());
        }
    }
}
