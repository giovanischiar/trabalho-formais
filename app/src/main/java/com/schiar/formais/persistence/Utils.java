package com.schiar.formais.persistence;

/**
 * Created by giovani on 21/10/16.
 */
public class Utils {
    public static String toString(String[][] table) {
        String stringTable = "";
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[0].length; j++) {
                stringTable += table[i][j];
                stringTable += "&#8203;";
            }
            if (i < table.length-1) {
                stringTable += "\n";
            }
        }
        return  stringTable;
    }

    public static String[][] fromString(String stringTable) {
        String[] rows = stringTable.split("\n");
        String[][] table = new String[rows.length][rows[0].split("&#8203;").length];
        for(int i = 0; i < table.length; i++) {
            String[] column = rows[i].split("&#8203;");
            for(int j = 0; j < table[0].length; j++) {
                table[i][j] = column[j];
            }
        }
        return table;
    }
}
