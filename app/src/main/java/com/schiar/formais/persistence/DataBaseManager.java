package com.schiar.formais.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.model.regex.Regex;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by giovani on 16/07/2015.
 */
public class DataBaseManager {

    private DBCreator database;

    public DataBaseManager(Context context) {
        database = new DBCreator(context);
    }

    public List<Integer> insertAutomaton(FiniteAutomaton... automata) {
        List<Integer> ids = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        for (FiniteAutomaton automaton : automata) {
            ContentValues valores;
            valores = new ContentValues();
            valores.put(DBCreator.getNAME(), automaton.getName());
            valores.put(DBCreator.getDESC(), automaton.getDesc());
            valores.put(DBCreator.isNDFA(), String.valueOf(automaton.isNdfa()));
            valores.put(DBCreator.getTABLE(), Utils.toString(automaton.generateArrayRepresentationOfTransitions()));
            valores.put(DBCreator.getIdFa(), automaton.getId());
            Integer id = (int)sqLiteDatabase.insert(DBCreator.getAUTOMATA(), null, valores);
            if (id < 0) {
                Log.wtf("magnamite", "Error inserting automata" + automaton.getName());
            }
            ids.add(id);
        }
        sqLiteDatabase.close();
        return ids;
    }

    public List<FiniteAutomaton> getAutomata() {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        List<FiniteAutomaton> automata = new ArrayList<>();
        String sql = "SELECT * FROM AUTOMATA";
        Cursor cursor;
        cursor = sqLiteDatabase.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
            FiniteAutomaton d = new FiniteAutomaton(cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3).equals("true"),
                    Utils.fromString(cursor.getString(4)));
            d.setId(cursor.getString(5));
            automata.add(d);
        }
        sqLiteDatabase.close();
        cursor.close();
        return automata;
    }

    public void removeAutomaton(String ID) {
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        String sql = "DELETE FROM AUTOMATA WHERE ID_FA = " + "\"" + ID + "\"";
        sqLiteDatabase.execSQL(sql);
        sqLiteDatabase.close();
    }

    public List<Regex> getRegices() {
        SQLiteDatabase sqLiteDatabase = database.getReadableDatabase();
        List<Regex> regices = new ArrayList<>();
        String sql = "SELECT * FROM REGICES";
        Cursor cursor;
        cursor = sqLiteDatabase.rawQuery(sql, new String[]{});
        while (cursor.moveToNext()) {
            Regex r = new Regex(cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3));
            r.setId(cursor.getString(4));
            regices.add(r);
        }
        sqLiteDatabase.close();
        cursor.close();
        return regices;
    }

    public List<Integer> insertRegex(Regex... regices) {
        List<Integer> ids = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        for (Regex regex : regices) {
            ContentValues valores;
            valores = new ContentValues();
            valores.put(DBCreator.getNAME(), regex.getName());
            valores.put(DBCreator.getDESC(), regex.getDescription());
            valores.put(DBCreator.getEXPRESSION(), regex.getExpression());
            valores.put(DBCreator.getID_REGEX(), regex.getId());
            Integer id = (int)sqLiteDatabase.insert(DBCreator.getREGICES(), null, valores);
            if (id < 0) {
                Log.wtf("magnamite", "Error inserting regex" + regex.getName());
            }
            ids.add(id);
        }
        sqLiteDatabase.close();
        return ids;
    }

    public void removeRegex(String ID) {
        SQLiteDatabase sqLiteDatabase = database.getWritableDatabase();
        String sql = "DELETE FROM REGICES WHERE ID_REGEX = " + "\"" + ID + "\"";
        sqLiteDatabase.execSQL(sql);
        sqLiteDatabase.close();
    }
}