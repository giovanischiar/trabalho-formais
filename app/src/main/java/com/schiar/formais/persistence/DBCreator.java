package com.schiar.formais.persistence;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User on 16/07/2015.
 */
public class DBCreator extends SQLiteOpenHelper {
    private static final String NAME_DATABASE = "database.db";
    private static final String AUTOMATA = "automata";
    private static final String ID = "_id";
    private static final String NAME = "name";
    private static final String DESC = "DESC";
    private static final String TABLE = "transitionTable";
    private static final String NDFA = "ndfa";
    private static final String ID_FA = "ID_FA";
    private static final String ID_REGEX = "ID_REGEX";
    private static final String EXPRESSION = "EXPRESSION";
    private static final String REGICES = "REGICES";

    private static final int VERSION = 1;

    public DBCreator(Context context){
        super(context, NAME_DATABASE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql1 = "CREATE TABLE " + AUTOMATA + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAME + " TEXT, "
                + DESC + " TEXT, "
                + NDFA + " TEXT, "
                + TABLE + " TEXT, "
                + ID_FA + " TEXT UNIQUE ON CONFLICT IGNORE "
                + ");";

        String sql2 = "CREATE TABLE " + REGICES + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAME + " TEXT, "
                + DESC + " TEXT, "
                + EXPRESSION + " TEXT, "
                + ID_REGEX + " TEXT UNIQUE ON CONFLICT IGNORE "
                + ");";

        db.execSQL(sql1);
        db.execSQL(sql2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public static String getID() {
        return ID;
    }

    public static String getNameDatabase() {
        return NAME_DATABASE;
    }

    public static String getAUTOMATA(){
        return AUTOMATA;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getDESC() {
        return DESC;
    }

    public static String isNDFA() {
        return NDFA;
    }

    public static String getIdFa() {
        return ID_FA;
    }

    public static String getTABLE() {
        return TABLE;
    }

    public static String getID_REGEX() {
        return ID_REGEX;
    }

    public static String getEXPRESSION() {
        return EXPRESSION;
    }

    public static String getREGICES() {
        return REGICES;
    }

}