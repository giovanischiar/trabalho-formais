package com.schiar.formais.controller;

/**
 * Created by Giovani on 13/11/2016.
 */

public interface TableValidator {

    boolean isTableValid(String[][] table, boolean ndfa);

}
