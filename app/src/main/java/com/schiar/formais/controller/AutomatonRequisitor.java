package com.schiar.formais.controller;

import com.schiar.formaisLib.model.FiniteAutomaton;

import java.util.List;

/**
 * Created by Giovani on 13/11/2016.
 */

public interface AutomatonRequisitor {
    List<FiniteAutomaton> requireAutomata();
}
