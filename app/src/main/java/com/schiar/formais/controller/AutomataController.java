package com.schiar.formais.controller;

import android.content.Context;

import com.schiar.formais.persistence.DataBaseManager;
import com.schiar.formaisLib.algorithm.Determinizator;
import com.schiar.formaisLib.algorithm.Minimizer;
import com.schiar.formaisLib.algorithm.SetOperations;
import com.schiar.formaisLib.model.FiniteAutomaton;
import com.schiar.formaisLib.util.TransitionTableVerifier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Giovani on 17/10/2016.
 */
public class AutomataController implements TableValidator, AutomatonSaver, AutomatonOperator, AutomatonRequisitor {

    private List<FiniteAutomaton> automata;
    private DataBaseManager dbCtrl;

    public AutomataController(Context c) {
        automata = new ArrayList<>();
        dbCtrl = new DataBaseManager(c);
        automata = dbCtrl.getAutomata();
    }

    public boolean testFA(String name, String word) {
        FiniteAutomaton fa = getAutomatonByID(name);
        return fa != null && fa.reconizeWord(word);
    }

    private FiniteAutomaton getAutomatonByID(String id) {
        for(FiniteAutomaton a : automata) {
            if(a.getId().equals(id)) {
                return a;
            }
        }
        return null;
    }

    public void delAutomaton(String name) {
        FiniteAutomaton toRemove = getAutomatonByID(name);
        if(toRemove != null) {
            automata.remove(toRemove);
            dbCtrl.removeAutomaton(name);
        }
    }

    @Override
    public boolean addAutomata(String oldId, String id, String name, String desc, boolean isNDFA, String[][] table) {
        if(table == null) {
            return false;
        }
        TransitionTableVerifier tv = new TransitionTableVerifier(table, isNDFA);
        if(tv.getErrors().size() > 0) {
            return false;
        }
        if(oldId.equals("")) {
            FiniteAutomaton automaton = new FiniteAutomaton(name, desc, isNDFA, table);
            automata.add(automaton);
            dbCtrl.insertAutomaton(automaton);
            return true;
        }else if(!oldId.equals(id)) {
            FiniteAutomaton automaton = new FiniteAutomaton(name, desc, isNDFA, table);
            automaton.setId(id);
            List<FiniteAutomaton> automataCopy = new ArrayList<>(automata);
            for(FiniteAutomaton a : automataCopy) {
                if(a.getId().equals(oldId)) {
                    int i = automata.indexOf(a);
                    automata.remove(i);
                    dbCtrl.removeAutomaton(oldId);
                    automata.add(i, automaton);
                    dbCtrl.insertAutomaton(automaton);
                    return true;
                }
            }
        }
        return false;
    }

    public List<FiniteAutomaton> requireAutomata() {
        automata = dbCtrl.getAutomata();
        Collections.sort(automata, new Comparator<FiniteAutomaton>() {
            @Override
            public int compare(FiniteAutomaton lhs, FiniteAutomaton rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        return automata;
    }

    public List<String> sentenceThatFARecognize(String name, int n) {
        FiniteAutomaton automaton = getAutomatonByID(name);
        if(automaton != null) {
            return automaton.allSentencesFrom0ToNThatFARecognize(n);
        }
        return new ArrayList<>();
    }

    public void determinizeAndAddAutomata(String name) {
        FiniteAutomaton automaton = getAutomatonByID(name);
        if(automaton == null) {
            return;
        }
        FiniteAutomaton determinizedAutomaton = Determinizator.determinize(automaton);
        automata.add(determinizedAutomaton);
        dbCtrl.insertAutomaton(determinizedAutomaton);
    }

    public void addIntersectionOfAutomata(String fa1, String fa2) {
        FiniteAutomaton automaton1 = getAutomatonByID(fa1);
        FiniteAutomaton automaton2 = getAutomatonByID(fa2);
        if(automaton1 == null || automaton2 == null) {
            return;
        }
        FiniteAutomaton intersectionedAutomaton = SetOperations.intersectionOfTwoAutomata(automaton1, automaton2);
        automata.add(intersectionedAutomaton);
        dbCtrl.insertAutomaton(intersectionedAutomaton);
    }

    public boolean compareAutomata(String fa1, String fa2) {
        FiniteAutomaton automaton1 = getAutomatonByID(fa1);
        FiniteAutomaton automaton2 = getAutomatonByID(fa2);
        return !(automaton1 == null || automaton2 == null) && automaton1.equivalent(automaton2);
    }

    public void minimizeAndAddAutomaton(String id) {
        FiniteAutomaton automaton = getAutomatonByID(id);
        if(automaton != null) {
            FiniteAutomaton automatonMinimized = Minimizer.minimize(automaton);
            automata.add(automatonMinimized);
            dbCtrl.insertAutomaton(automatonMinimized);
        }
    }

    @Override
    public boolean isTableValid(String[][] table, boolean ndfa) {
        return new TransitionTableVerifier(table, ndfa).getErrors().isEmpty();
    }


    public boolean isAutomatonNDFA(String id) {
        FiniteAutomaton automaton = getAutomatonByID(id);
        return automaton != null && automaton.isNdfa();
    }
}
