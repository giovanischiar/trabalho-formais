package com.schiar.formais.controller;

import android.content.Context;

import com.schiar.formais.persistence.DataBaseManager;
import com.schiar.formaisLib.algorithm.DiSimone;
import com.schiar.formaisLib.model.regex.Regex;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Giovani on 14/11/2016.
 */

public class RegexController implements RegexOperator, RegexSaver, RegexRequisitor {
    private List<Regex> regices;
    private DataBaseManager dbCtrl;

    public RegexController(Context c) {
        regices = new ArrayList<>();
        dbCtrl = new DataBaseManager(c);
        regices = dbCtrl.getRegices();
    }

    public boolean addRegex(String oldId, String id, String name, String desc, String expression) {
        if (oldId.equals("")) {
            Regex regex = new Regex(name, desc, expression);
            regices.add(regex);
            dbCtrl.insertRegex(regex);
            return true;
        } else if (!oldId.equals(id)) {
            Regex regex = new Regex(name, desc, expression);
            regex.setId(id);
            List<Regex> regicesCopy = new ArrayList<>(regices);
            for (Regex a : regicesCopy) {
                if(a.getId().equals(oldId)) {
                    int i = regices.indexOf(a);
                    regices.remove(i);
                    dbCtrl.removeRegex(oldId);
                    regices.add(i, regex);
                    dbCtrl.insertRegex(regex);
                    return true;
                }
            }
        }
        return false;
    }

    public void delRegex(String id) {
        Regex toRemove = getRegexByID(id);
        if(toRemove != null) {
            regices.remove(toRemove);
            dbCtrl.removeRegex(id);
        }
    }

    public List<Regex> requireRegices() {
        return regices;
    }

    private Regex getRegexByID(String id) {
        for(Regex a : regices) {
            if(a.getId().equals(id)) {
                return a;
            }
        }
        return null;
    }

    public void convertToAutomata(String id) {
        Regex regex = getRegexByID(id);
        if (regex != null) {
            dbCtrl.insertAutomaton(DiSimone.buildAutomaton(regex));
        }
    }
}
