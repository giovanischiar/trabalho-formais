package com.schiar.formais.controller;

import java.util.List;

/**
 * Created by Giovani on 13/11/2016.
 */
public interface AutomatonOperator {
    void delAutomaton(String id);
    boolean testFA(String name, String word);
    List<String> sentenceThatFARecognize(String name, int n);
    boolean compareAutomata(String fa1, String fa2);
    void addIntersectionOfAutomata(String fa1, String fa2);
    void determinizeAndAddAutomata(String id);
    void minimizeAndAddAutomaton(String id);
    boolean isAutomatonNDFA(String id);
}
