package com.schiar.formais.controller;

/**
 * Created by Giovani on 14/11/2016.
 */

public interface RegexOperator {
    void delRegex(String id);
    void convertToAutomata(String id);
}
