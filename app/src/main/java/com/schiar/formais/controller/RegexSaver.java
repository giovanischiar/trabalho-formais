package com.schiar.formais.controller;

/**
 * Created by Giovani on 14/11/2016.
 */

public interface RegexSaver {
    boolean addRegex(String oldId, String id, String name, String desc, String expression);
}
