package com.schiar.formais.controller;

import com.schiar.formaisLib.model.regex.Regex;

import java.util.List;

/**
 * Created by Giovani on 14/11/2016.
 */

public interface RegexRequisitor {
    List<Regex> requireRegices();
}
