package com.schiar.formais.controller;

/**
 * Created by Giovani on 13/11/2016.
 */

public interface AutomatonSaver {
    boolean addAutomata(String oldId, String id, String name, String desc, boolean isNDFA, String[][] table);
}
